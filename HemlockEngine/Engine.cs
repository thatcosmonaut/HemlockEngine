﻿using System;
using System.Collections.Generic;

namespace HemlockEngine
{
    public abstract class Engine
    {
        protected HashSet<Type> componentTypes;
        protected List<Entity> trackedEntities = new List<Entity>();

        public Engine(EntityManager entityManager)
        {
            entityManager.RegisterEngine(this);
        }

        public void CheckAndRegisterEntity(Entity entity)
        {
            if (trackedEntities.Contains(entity)) { return; }

            bool hasAllComponents = true;

            foreach (Type componentType in componentTypes)
            {
                hasAllComponents &= entity.HasComponent(componentType);
            }

            if (hasAllComponents)
            {
                trackedEntities.Add(entity);
            }
        }
    }
}
